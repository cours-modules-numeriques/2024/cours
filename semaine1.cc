#include <vector>
#include <cmath>
#include <iostream>

// comment: my function to calculate the sum of an array
double mySum(const std::vector<double> &in) {
    double s = 0.0;
    for (size_t n=0; n<in.size(); n++) {
        s += in[n];
    }
    return s;
}

void ScopeOfVariables() {
    int k1=1, k2=-2, k3=-1;
    for (int k2=0; k2<k1; k2+=k3+k1) {
        int k3 = 2*k2;
        k1++;
    }
    std::cout << k1 << " " << k2 << " " << k3 << std::endl;
}

void UnsignedOrSigned() {
    unsigned i = 1;
    int j = 1;
    std::cout << "Unsigned: " << i << " " << i-1 << " " << i-2 << " " << std::endl;
    std::cout << "Signed: " << j << " " << j-1 << " " << j-2 << " " << std::endl;
    std::cout << "Positive? " << (i-2>0) << " " << (j-2>0) << std::endl;
}

int main(int argc, char **argv) {
// ------- Je teste la somme du vecteur myVec avec la fonction mySum:
    std::vector<double> myVec = {1, 2, 3./7., 5/3, M_PI, std::cos(0.25*M_PI)};
    std::cout << myVec[2] << " " << myVec[3] << " " << myVec[4] << std::endl;
    std::cout << mySum(myVec) << std::endl;

// ------- Je teste les "particularismes" du C++:
    std::cout << "*** Variables locales ou globales ***\n";
    ScopeOfVariables();
    std::cout << "*** Entiers signed / unsigned ***\n";
    UnsignedOrSigned();

    return 0;
}
