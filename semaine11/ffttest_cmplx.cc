#include <cmath>
#include <complex>
#include <fstream>
#include <iostream>
#include <vector>

#include <fftw3.h>

using namespace std;
typedef fftw_complex fwc;
typedef complex<double> cmplx;

int main(int argc, char **argv) {
    size_t nsize = 210; // 44100 = (2*3*5*7)**2
    vector<cmplx> f(nsize, 0.);
    fftw_plan fftfwd = fftw_plan_dft_1d(nsize, 0, 0, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_plan fftbwd = fftw_plan_dft_1d(nsize, 0, 0, FFTW_BACKWARD, FFTW_ESTIMATE);

    for (double nx=10; nx<30; nx++) {
        cmplx zz = -0.05*(nx-19.5)*(nx-19.5)+(nx-10.)*0.3i;
        f[nx] = exp(zz);
    }

    for (auto I : f) cout << I.real() << " ";
    cout << endl;
    for (auto I : f) cout << I.imag() << " ";
    cout << endl;

    fftw_execute_dft(fftfwd, (fwc*)(f.data()), (fwc*)(f.data()));

    for (auto I : f) cout << I.real() << " ";
    cout << endl;
    for (auto I : f) cout << I.imag() << " ";
    cout << endl;

    fftw_execute_dft(fftbwd, (fwc*)(f.data()),(fwc*)(f.data()));

    for (auto I : f) cout << I.real() << " ";
    cout << endl;
    for (auto I : f) cout << I.imag() << " ";
    cout << endl;

    for (auto &I : f) I=0;
    f[5] = 1;
    f[nsize-5] = 1;
    for (auto I : f) cout << I.real() << " ";
    cout << endl;
    for (auto I : f) cout << I.imag() << " ";
    cout << endl;

    fftw_execute_dft(fftbwd, (fwc*)(f.data()), (fwc*)(f.data()));


    for (auto I : f) cout << I.real() << " ";
    cout << endl;
    for (auto I : f) cout << I.imag() << " ";
    cout << endl;

    fftw_destroy_plan(fftfwd);
    fftw_destroy_plan(fftbwd);
    return 0;
};
