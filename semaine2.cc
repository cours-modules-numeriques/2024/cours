#include <list>
#include <map>
#include <algorithm>
#include <vector>
#include <string>
#include <iostream>

using namespace std;

void dovector() {
    vector<int> A;
    for (int n=0; n<999; n++) A.push_back(n*(n-1)/2);
    cout << A.size() << endl;
    cout << A.front() << " " << A[202] << " " << A[67] << " " << A.back() << endl;
    vector<int>::iterator  I = A.insert(A.begin()+13, 0);
    A.erase(A.end()-21, A.end()-10);
    cout << A.size() << "\n";
    A.clear();
    cout << A.size() << "\n";
}

void dolist() {
    list<int> A;
    for (int n = 0; n < 100; n++) A.push_back(2*n);
    int n = 0; 
    for (auto I=A.begin(); I!=A.end(); I++) {I = A.insert(++I, 2*n+1); n++;}
    cout << A.size() << "\n";
    for (auto I=A.begin(); distance(A.begin(), I)<10; I++) cout << *I << " ";
}
void domap() {
    map<char, int> A;
    string test("Universite de Geneve");
    for (auto I : test) {
        if (A.count(I) == 0) A[I] = 1;
        else A[I]++;
    }
    for (auto I : A) cout << "|" << I.second << ":" << I.first;
}

bool positive(int i) {return i>0;}
int incr(int i) {return i+1;}
void doalgo() {
    vector<int> v{-3, 5, 2, -1, 3};
    auto I1 = find(v.begin(), v.end(), 3);
    auto I2 = find_if(v.begin(), v.end(), positive);
    cout << I1-v.begin() << " " << *I2 << "\n";

    sort(v.begin(), v.end());
    for (auto I : v) cout << I << " "; cout << "\n";

    transform(v.begin(), v.end(), v.begin(), incr);
    for (auto I : v) cout << I << " "; cout << "\n";
}
void dostreams() {
    string user_data;
    cin >> user_data;
    cout << "You typed " << user_data.size() << " characters: " << user_data;
}

int main(int argc, char **argv) {
/* vector */
    cout << "***** vector *****\n";
    dovector();
    cout << "\n";
/* list */
    cout << "***** list *****\n";
    dolist();
    cout << "\n";
/* map */
    cout << "***** map *****\n";
    domap();
    cout << "\n";
/* algorithm */
    cout << "***** algorithm *****\n";
    doalgo();
    cout << "\n";
/* streams */
    cout << "***** streams *****\n";
    dostreams();
    cout << "\n";

//---- compter et afficher les arguments
    cout << argc << "\n-------\n";
    for (int n=0; n<argc; n++)
        cout << n << " " << argv[n] << "\n";
    cout << "\n-------\n";
//---- manipuler les arguments
    int alpha = stoi(argv[1]);
    alpha *= 2;
    string nom(argv[2]);
    reverse(nom.begin(), nom.end());
    cout << alpha << " " << nom << "\n";
    return 0;
}
