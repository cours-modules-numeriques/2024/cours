#include "main.h"

using namespace std;
using namespace boost::program_options;

double last;
double next() {
    last = last*(1-last)*3.975;
    return last;
}

void do_random() {
    last = .78;
    for (int n=0; n<1000; n++) {
        double x = next();
        cout << x << " ";
    }
    cout << endl;
}

mt19937 rng;
random_device rd;

void do_random_std() {
    binomial_distribution<> dbinom(20,.256);
    for (int n=0; n<10000; n++) cout << dbinom(rng) << " ";
    cout << endl;
}

int main(int argc, char **argv) {
    /* générateur maison */
    do_random();

    /* générateur standard */
    int seed = rd();
    rng.seed(seed);
    do_random_std();

    /* arguments du programme avec boost */
    options_description desc("Ce programme lit les options");
    int size = 1;
    std::string name = "Joe";
    desc.add_options()
        ("help,h", "Show help message")
        ("nom,n", value<std::string>(&name)->default_value(name), "Mon nom")
        ("size,s", value<int>(&size)->default_value(size), "Ma taille");
    variables_map vm;
    store(parse_command_line(argc, argv, desc), vm);
    notify(vm);
    if (vm.count("help")) {
        std::cout << desc << "\n";
        return 0;
    }
    std::cout  << "Bonjour " << name << " : " << size << std::endl;
    return 0;
}
