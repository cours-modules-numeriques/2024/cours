#include "main.h"

using namespace std;

class rotation {
public:
    rotation(double a=0) : alpha(a) {}
    rotation& operator*(const rotation &r) {
        alpha += r.alpha;
        return *this;
    }
    rotation& operator*=(rotation r) {
        alpha += r.alpha;
        return *this;
    }
    void operator()(vector<double> &x) {
        double xx = cos(alpha)*x[0]-sin(alpha)*x[1];
        x[1] = sin(alpha)*x[0]+cos(alpha)*x[1];
        x[0] = xx;
    }
    double angle() const {return alpha;}
    
private:
    double alpha;
};

vector<string> manystr(10);
vector<rotation> manyrot;

int main(int, char**) {
    double x(3.1415);
    double y = 3.1415;
    double z = y;
    z *= x;
    x += y;
    
    string s1("part1");
    string s2 = "part2";
    s1 += s2;

    valarray<double> v1(.1, 2);
    valarray<double> v2{2.2, 5.7};
    v2 *= v1;
    
    cout << x << " " << y << " " << z << endl
         << s1 << " " << s2 << endl
         << v2[0] << " " << v2[1] << endl;


    rotation R1(-1.056);
    rotation R2(M_PI/3.);
    R1 = R1*R2;
    R2 *= R2;
    vector<double> x2{1.1, 3.2};
    R2(x2);
    cout << R1.angle() << " " << R2.angle() << endl
         << x2[0] << " " << x2[1] << endl;

    manystr[0] = "hahaha";
    manystr[4] = "All the world's a stage, and all the men and women merely players.";
    manystr[4] += " They have their exits and their entrances; And one man in his time plays many parts.";
    cout << manystr.size() << endl;
    for (auto I : manystr) cout << I << endl;

    manyrot.push_back(0.01);
    manyrot.push_back(M_PI/sqrt(2));
    manyrot.push_back(R1);
    manyrot[1](x2);
    cout << x2[0] << " " << x2[1] << endl;
    
    return 0;
}
