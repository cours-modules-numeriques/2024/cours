#include <algorithm>
#include <iomanip>
#include <iostream>
#include <random>
#include <string>
#include <vector>

double xdiv(0.9969519192), xmult(1.0/xdiv);
double div(double x) {return x/xdiv;}
double mult(double x) {return x*xmult;}

size_t N(1000000); // 1 Million

std::random_device rd;
std::mt19937 rng = std::mt19937(rd());

/* Fill “rands” with random numbers */
/* indices = random permutation of 0,…,(N-1) */

std::vector< double > rands(N);
std::vector< int > indices(N);

int main() {
    std::normal_distribution<> norm;
    for (auto &I : rands) I = norm(rng);
    std::iota(indices.begin(), indices.end(), 1);
    std::shuffle(indices.begin(), indices.end(), rng);
    for (int n=0; n<1000; n++) {
#ifdef _MULT_
        for (auto &I : rands) I *= xmult;
#elif defined _DIV_
        for (auto &I : rands) I /= xdiv;
#elif defined _MULT_FUNC_
        for (auto &I : rands) I = mult(I);
#elif defined _DIV_FUNC_
        for (auto &I : rands) I = div(I);
#elif defined _DIV2_
        for (int k=0; k<N; k++) rands[k] /= xdiv;
#elif defined _MF2_
        for (int k=0; k<N; k++) rands[k] = mult(rands[k]);
#elif defined _DF2_
        for (int k=0; k<N; k++) rands[k] = div(rands[k]);
#elif defined _SHUFFLE_
        for (auto k : indices) rands[k] *= xmult;
#elif defined _ORDER_
        for (int k=0; k<N; k++) rands[k] *= xmult;
#elif defined _MIF_
        for (int k=0; k<N; k++) 
            if (rands[k]>0.) rands[k] *= xmult;
#elif defined _DIF_
        for (int k=0; k<N; k++) 
            if (rands[k]>0.) rands[k] /= xdiv;
#endif
    }
    return 0;
}
